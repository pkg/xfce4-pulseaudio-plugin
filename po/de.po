# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Andreas Eitel <github-aneitel@online.de>, 2020,2022
# Tobias Bannert <tobannert@gmail.com>, 2016-2017
# Vinzenz Vietzke <vinz@vinzv.de>, 2017-2018
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-09-11 12:56+0200\n"
"PO-Revision-Date: 2015-10-05 20:18+0000\n"
"Last-Translator: Andreas Eitel <github-aneitel@online.de>, 2020,2022\n"
"Language-Team: German (http://www.transifex.com/xfce/xfce-panel-plugins/language/de/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../panel-plugin/pulseaudio.desktop.in.in.h:1
msgid "PulseAudio Plugin"
msgstr "PulseAudio-Modul"

#: ../panel-plugin/pulseaudio.desktop.in.in.h:2
#: ../panel-plugin/pulseaudio-plugin.c:256
msgid "Adjust the audio volume of the PulseAudio sound system"
msgstr "Die Lautstärke des PulseAudio-Klangsystems anpassen"

#: ../panel-plugin/pulseaudio-dialog.glade.h:1
msgid "PulseAudio Panel Plugin"
msgstr "PulseAudio-Leistenmodul"

#: ../panel-plugin/pulseaudio-dialog.glade.h:2
msgid "Enable keyboard _shortcuts for volume control"
msgstr "_Tastaturkürzel für Lautstärkeregelung aktivieren"

#: ../panel-plugin/pulseaudio-dialog.glade.h:3
msgid ""
"Enables volume control using multimedia keys. Make sure no other application"
" that listens to these keys (e.g. xfce4-volumed) is running in the "
"background."
msgstr "Aktiviert die Lautstärkeregelung mit Hilfe von Multimediatasten. Bitte stellen Sie sicher, dass keine andere Anwendung, die auf die Schlüssel (z. B. xfce4-volumed) zugreift, im Hintergrund ausgeführt wird."

#: ../panel-plugin/pulseaudio-dialog.glade.h:4
msgid "Show _notifications when volume changes"
msgstr "_Benachrichtigungen anzeigen, wenn sich die Lautstärke ändert"

#: ../panel-plugin/pulseaudio-dialog.glade.h:5
msgid "Enables on-screen volume notifications."
msgstr "Bildschirmbenachrichtigungen der Lautstärke aktivieren"

#: ../panel-plugin/pulseaudio-dialog.glade.h:6
msgid "Play system _sound when volume changes"
msgstr "_Systemton bei Lautstärkeänderung abspielen"

#: ../panel-plugin/pulseaudio-dialog.glade.h:7
msgid ""
"Enables audio feedback when using multimedia keys to change the volume."
msgstr "Aktiviert die akustische Rückmeldung bei der Verwendung von Multimediatasten zur Änderung der Lautstärke."

#: ../panel-plugin/pulseaudio-dialog.glade.h:8
msgid "Behaviour"
msgstr "Verhalten"

#: ../panel-plugin/pulseaudio-dialog.glade.h:9
msgid "Audio _Mixer"
msgstr "Ton_mischer"

#: ../panel-plugin/pulseaudio-dialog.glade.h:10
msgid ""
"Audio mixer command that can be executed from the context menu, e.g. "
"\"pavucontrol\", \"unity-control-center sound\"."
msgstr "Klangmischerbefehl, der aus dem Kontextmenü ausgeführt werden kann, z.B. »pavucontrol«, »unity-control-center sound«."

#: ../panel-plugin/pulseaudio-dialog.glade.h:11
msgid "_Run Audio Mixer..."
msgstr "Tonmischer _starten …"

#: ../panel-plugin/pulseaudio-dialog.glade.h:12
msgid "Sound Settings"
msgstr "Klangeinstellungen"

#: ../panel-plugin/pulseaudio-dialog.glade.h:13
msgid "General"
msgstr "Allgemein"

#: ../panel-plugin/pulseaudio-dialog.glade.h:14
msgid "Control Playback of Media Players"
msgstr "Wiedergabe von Medienspielern steuern"

#: ../panel-plugin/pulseaudio-dialog.glade.h:15
msgid "Enable multimedia keys for playback control"
msgstr "Multimediatasten für die Wiedergabesteuerung aktivieren"

#: ../panel-plugin/pulseaudio-dialog.glade.h:16
msgid "Enable experimental window focus support"
msgstr "Experimentelle Unterstützung für Fensterfokus aktivieren"

#: ../panel-plugin/pulseaudio-dialog.glade.h:17
msgid "Title"
msgstr "Titel"

#: ../panel-plugin/pulseaudio-dialog.glade.h:18
msgid "Hidden"
msgstr "Versteckt"

#: ../panel-plugin/pulseaudio-dialog.glade.h:19
msgid "Clear Known Items"
msgstr "Bekannte Einträge leeren"

#: ../panel-plugin/pulseaudio-dialog.glade.h:20
msgid "Please restart your panel for additional players to be displayed."
msgstr "Bitte starten Sie Ihre Leiste neu, damit zusätzliche Medienspieler angezeigt werden."

#: ../panel-plugin/pulseaudio-dialog.glade.h:21
msgid "Known Media Players"
msgstr "Bekannte Medienspieler"

#: ../panel-plugin/pulseaudio-dialog.glade.h:22
msgid "Media Players"
msgstr "Medienspieler"

#: ../panel-plugin/pulseaudio-plugin.c:258
msgid "Copyright © 2014-2022 Andrzej Radecki et al.\n"
msgstr "Urheberrecht © 2014-2022 Andrzej Radecki et al.\n"

#: ../panel-plugin/pulseaudio-dialog.c:142
#: ../panel-plugin/pulseaudio-menu.c:264
#, c-format
msgid ""
"<big><b>Failed to execute command \"%s\".</b></big>\n"
"\n"
"%s"
msgstr "<big><b>Ausführen von Befehl »%s« ist fehlgeschlagen.</b></big>\n\n%s"

#: ../panel-plugin/pulseaudio-dialog.c:145
#: ../panel-plugin/pulseaudio-menu.c:267
msgid "Error"
msgstr "Fehler"

#: ../panel-plugin/pulseaudio-button.c:364
#: ../panel-plugin/pulseaudio-notify.c:199
#, c-format
msgid "Not connected to the PulseAudio server"
msgstr "Nicht mit dem PulseAudio-Server verbunden"

#: ../panel-plugin/pulseaudio-button.c:366
#, c-format
msgid "Volume %d%% (muted)"
msgstr "Lautstärke %d%% (Stumm)"

#: ../panel-plugin/pulseaudio-button.c:368
#, c-format
msgid "Volume %d%%"
msgstr "Lautstärke %d%%"

#: ../panel-plugin/pulseaudio-menu.c:507
msgid "Output"
msgstr "Ausgabe"

#: ../panel-plugin/pulseaudio-menu.c:546
msgid "Input"
msgstr "Eingabe"

#: ../panel-plugin/pulseaudio-menu.c:634
msgid "Choose Playlist"
msgstr "Wiedergabeliste wählen"

#. Audio mixers
#: ../panel-plugin/pulseaudio-menu.c:671
msgid "_Audio mixer..."
msgstr "To_nmischer …"

#: ../panel-plugin/pulseaudio-notify.c:201
#, c-format
msgid "Volume %d%c (muted)"
msgstr "Lautstärke %d%c (Stumm)"

#: ../panel-plugin/pulseaudio-notify.c:203
#, c-format
msgid "Volume %d%c"
msgstr "Lautstärke %d%c"

#: ../panel-plugin/mprismenuitem.c:363 ../panel-plugin/mprismenuitem.c:436
#: ../panel-plugin/mprismenuitem.c:810
msgid "Not currently playing"
msgstr "Aktuell keine Wiedergabe"
