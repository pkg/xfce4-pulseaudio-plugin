# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Arve Eriksson <031299870@telia.com>, 2017
# Carlos Dz <cls567@tuta.io>, 2020
# Luna Jernberg <bittin@cafe8bitar.se>, 2022
# Påvel Nicklasson <pavel2@frimix.se>, 2017
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-09-11 12:56+0200\n"
"PO-Revision-Date: 2015-10-05 20:18+0000\n"
"Last-Translator: Luna Jernberg <bittin@cafe8bitar.se>, 2022\n"
"Language-Team: Swedish (http://www.transifex.com/xfce/xfce-panel-plugins/language/sv/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sv\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../panel-plugin/pulseaudio.desktop.in.in.h:1
msgid "PulseAudio Plugin"
msgstr "PulseAudio insticksprogram"

#: ../panel-plugin/pulseaudio.desktop.in.in.h:2
#: ../panel-plugin/pulseaudio-plugin.c:256
msgid "Adjust the audio volume of the PulseAudio sound system"
msgstr "Justera ljudvolymen på PulseAudios ljudsystem"

#: ../panel-plugin/pulseaudio-dialog.glade.h:1
msgid "PulseAudio Panel Plugin"
msgstr "PulseAudio Panelinsticksprogram"

#: ../panel-plugin/pulseaudio-dialog.glade.h:2
msgid "Enable keyboard _shortcuts for volume control"
msgstr "Aktivera tangentbord_sgenvägar för volymkontroll"

#: ../panel-plugin/pulseaudio-dialog.glade.h:3
msgid ""
"Enables volume control using multimedia keys. Make sure no other application"
" that listens to these keys (e.g. xfce4-volumed) is running in the "
"background."
msgstr "Aktiverar volymkontroll med multimediatangenter. Förvissa dig om att inget annat program som lyssnar till dessa tangenter (t. ex. xfce4-volumed) kör i bakgrunden."

#: ../panel-plugin/pulseaudio-dialog.glade.h:4
msgid "Show _notifications when volume changes"
msgstr "Visa meddelanden _när volymen ändras"

#: ../panel-plugin/pulseaudio-dialog.glade.h:5
msgid "Enables on-screen volume notifications."
msgstr "Aktiverar volymmeddelanden på skärmen."

#: ../panel-plugin/pulseaudio-dialog.glade.h:6
msgid "Play system _sound when volume changes"
msgstr "Spela system _ljud då volymen ändras"

#: ../panel-plugin/pulseaudio-dialog.glade.h:7
msgid ""
"Enables audio feedback when using multimedia keys to change the volume."
msgstr "Aktiverar ljudåterkoppling när du använder multimediaknappar för att ändra volymen."

#: ../panel-plugin/pulseaudio-dialog.glade.h:8
msgid "Behaviour"
msgstr "Beteende"

#: ../panel-plugin/pulseaudio-dialog.glade.h:9
msgid "Audio _Mixer"
msgstr "Ljud_mixer"

#: ../panel-plugin/pulseaudio-dialog.glade.h:10
msgid ""
"Audio mixer command that can be executed from the context menu, e.g. "
"\"pavucontrol\", \"unity-control-center sound\"."
msgstr "Ljudmixerkommando som kan köras från kontextmenyn, t. ex. \"pavucontrol\", \"unity-control-center sound\"."

#: ../panel-plugin/pulseaudio-dialog.glade.h:11
msgid "_Run Audio Mixer..."
msgstr "Kö_r Ljudmixer..."

#: ../panel-plugin/pulseaudio-dialog.glade.h:12
msgid "Sound Settings"
msgstr "Ljudinställningar"

#: ../panel-plugin/pulseaudio-dialog.glade.h:13
msgid "General"
msgstr "Allmänt"

#: ../panel-plugin/pulseaudio-dialog.glade.h:14
msgid "Control Playback of Media Players"
msgstr "Kontrollera uppspelning av mediaspelare"

#: ../panel-plugin/pulseaudio-dialog.glade.h:15
msgid "Enable multimedia keys for playback control"
msgstr "Aktivera multimediatangenter för uppspelningskontroll"

#: ../panel-plugin/pulseaudio-dialog.glade.h:16
msgid "Enable experimental window focus support"
msgstr "Aktivera experimentellt stöd för fönsterfokus"

#: ../panel-plugin/pulseaudio-dialog.glade.h:17
msgid "Title"
msgstr "Titel"

#: ../panel-plugin/pulseaudio-dialog.glade.h:18
msgid "Hidden"
msgstr "Dolt"

#: ../panel-plugin/pulseaudio-dialog.glade.h:19
msgid "Clear Known Items"
msgstr "Rensa kända objekt"

#: ../panel-plugin/pulseaudio-dialog.glade.h:20
msgid "Please restart your panel for additional players to be displayed."
msgstr "Starta om din panel för att visa ytterligare spelare"

#: ../panel-plugin/pulseaudio-dialog.glade.h:21
msgid "Known Media Players"
msgstr "Kända mediaspelare"

#: ../panel-plugin/pulseaudio-dialog.glade.h:22
msgid "Media Players"
msgstr "Mediaspelare"

#: ../panel-plugin/pulseaudio-plugin.c:258
msgid "Copyright © 2014-2022 Andrzej Radecki et al.\n"
msgstr "Copyright © 2014-2022 Andrzej Radecki m.m.\n"

#: ../panel-plugin/pulseaudio-dialog.c:142
#: ../panel-plugin/pulseaudio-menu.c:264
#, c-format
msgid ""
"<big><b>Failed to execute command \"%s\".</b></big>\n"
"\n"
"%s"
msgstr "<big><b>Det gick inte att köra kommandot  \"%s\".</b></big>\n\n%s"

#: ../panel-plugin/pulseaudio-dialog.c:145
#: ../panel-plugin/pulseaudio-menu.c:267
msgid "Error"
msgstr "Fel"

#: ../panel-plugin/pulseaudio-button.c:364
#: ../panel-plugin/pulseaudio-notify.c:199
#, c-format
msgid "Not connected to the PulseAudio server"
msgstr "Inte ansluten till PulseAudio servern"

#: ../panel-plugin/pulseaudio-button.c:366
#, c-format
msgid "Volume %d%% (muted)"
msgstr "Volym %d%% (tystad)"

#: ../panel-plugin/pulseaudio-button.c:368
#, c-format
msgid "Volume %d%%"
msgstr "Volym %d%%"

#: ../panel-plugin/pulseaudio-menu.c:507
msgid "Output"
msgstr "Utmatning"

#: ../panel-plugin/pulseaudio-menu.c:546
msgid "Input"
msgstr "Inmatning"

#: ../panel-plugin/pulseaudio-menu.c:634
msgid "Choose Playlist"
msgstr "Välj spellista"

#. Audio mixers
#: ../panel-plugin/pulseaudio-menu.c:671
msgid "_Audio mixer..."
msgstr "Lj_udmixer..."

#: ../panel-plugin/pulseaudio-notify.c:201
#, c-format
msgid "Volume %d%c (muted)"
msgstr "Volym %d%c (tystad)"

#: ../panel-plugin/pulseaudio-notify.c:203
#, c-format
msgid "Volume %d%c"
msgstr "Volym %d%c"

#: ../panel-plugin/mprismenuitem.c:363 ../panel-plugin/mprismenuitem.c:436
#: ../panel-plugin/mprismenuitem.c:810
msgid "Not currently playing"
msgstr "Spelar inte för närvarande"
